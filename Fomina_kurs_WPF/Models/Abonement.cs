namespace Fomina_kurs_WPF.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Abonement")]
    public partial class Abonement
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Abonement()
        {
            Schedule = new HashSet<Schedule>();
        }

        public int Id { get; set; }

        public int Client_id { get; set; }

        public int Service_id { get; set; }

        public int Trainer_id { get; set; }

        public int Exercise_count { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date_of_sale { get; set; }

        public int Price { get; set; }

        public int Status_id { get; set; }

        public virtual Client Client { get; set; }

        public virtual Service Service { get; set; }

        public virtual STATUS STATUS { get; set; }

        public virtual Trainer Trainer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Schedule> Schedule { get; set; }
    }
}
