namespace Fomina_kurs_WPF.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class FitnessContext : DbContext
    {
        public FitnessContext()
            : base("name=FitnessContext")
        {
        }

        public virtual DbSet<Abonement> Abonement { get; set; }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<PERIOD> PERIOD { get; set; }
        public virtual DbSet<Person> Person { get; set; }
        public virtual DbSet<Schedule> Schedule { get; set; }
        public virtual DbSet<Service> Service { get; set; }
        public virtual DbSet<SEX> SEX { get; set; }
        public virtual DbSet<STATUS> STATUS { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<Trainer> Trainer { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<USER_TYPE> USER_TYPE { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Abonement>()
                .HasMany(e => e.Schedule)
                .WithRequired(e => e.Abonement)
                .HasForeignKey(e => e.Abonement_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.FIO)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .HasMany(e => e.Abonement)
                .WithRequired(e => e.Client)
                .HasForeignKey(e => e.Client_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PERIOD>()
                .Property(e => e.Period)
                .IsUnicode(false);

            modelBuilder.Entity<PERIOD>()
                .HasMany(e => e.Schedule)
                .WithRequired(e => e.PERIOD)
                .HasForeignKey(e => e.Period_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.FIO)
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .HasMany(e => e.Abonement)
                .WithRequired(e => e.Service)
                .HasForeignKey(e => e.Service_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Service>()
                .HasMany(e => e.Trainer)
                .WithRequired(e => e.Service)
                .HasForeignKey(e => e.Service_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SEX>()
                .Property(e => e.Sex1)
                .IsFixedLength();

            modelBuilder.Entity<SEX>()
                .HasMany(e => e.Client)
                .WithRequired(e => e.SEX)
                .HasForeignKey(e => e.Sex_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<STATUS>()
                .Property(e => e.Status1)
                .IsFixedLength();

            modelBuilder.Entity<STATUS>()
                .HasMany(e => e.Abonement)
                .WithRequired(e => e.STATUS)
                .HasForeignKey(e => e.Status_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Trainer>()
                .Property(e => e.FIO)
                .IsUnicode(false);

            modelBuilder.Entity<Trainer>()
                .HasMany(e => e.Abonement)
                .WithRequired(e => e.Trainer)
                .HasForeignKey(e => e.Trainer_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Login)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasOptional(e => e.Client)
                .WithRequired(e => e.User);

            modelBuilder.Entity<User>()
                .HasOptional(e => e.Person)
                .WithRequired(e => e.User);

            modelBuilder.Entity<User>()
                .HasOptional(e => e.Trainer)
                .WithRequired(e => e.User);

            modelBuilder.Entity<USER_TYPE>()
                .Property(e => e.User_type1)
                .IsFixedLength();

            modelBuilder.Entity<USER_TYPE>()
                .HasMany(e => e.User)
                .WithRequired(e => e.USER_TYPE)
                .HasForeignKey(e => e.User_type_Id)
                .WillCascadeOnDelete(false);
        }
    }
}
