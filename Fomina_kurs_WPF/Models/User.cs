namespace Fomina_kurs_WPF.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("User")]
    public partial class User
    {
        [Key]
        public int User_Id { get; set; }

        [Required]
        [StringLength(40)]
        public string Login { get; set; }

        [Required]
        [StringLength(40)]
        public string Password { get; set; }

        public int User_type_Id { get; set; }

        public virtual Client Client { get; set; }

        public virtual Person Person { get; set; }

        public virtual Trainer Trainer { get; set; }

        public virtual USER_TYPE USER_TYPE { get; set; }
    }
}
