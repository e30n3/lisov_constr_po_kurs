﻿using Fomina_kurs_WPF.BLL;
using Fomina_kurs_WPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Fomina_kurs_WPF.ViewModel
{
    class ScheduleViewModel : INotifyPropertyChanged
    {
        public int abonementId { get; }
        public bool isActive { get; }
        private FitnessContext db;
        public ObservableCollection<AbonementModel> Abonements { get; set; }
        public ObservableCollection<Schedule> AllSchedules { get; set; }
        public ObservableCollection<ScheduleModel> Schedules { get; set; }
        //public ObservableCollection<Schedule> schedule { get; set; }
        private Schedule selectedSchedule;
        private AbonementModel selectedAbonement;
        public Schedule SelectedSchedule
        {
            get { return selectedSchedule; }
            set
            {
                selectedSchedule = value;
                System.Windows.MessageBox.Show(selectedSchedule.Abonement_id.ToString());
                OnPropertyChanged("SelectedSchedule");
            }
        }
        public AbonementModel SelectedAbonement
        {
            get { return selectedAbonement; }
            set
            {
                selectedAbonement = value;
                OnPropertyChanged("SelectedAbonement");

                Schedules.Clear();
                List<Schedule> tmpSchedule = db.Schedule.Where(s => s.Abonement_id == selectedAbonement.Id).ToList();
                tmpSchedule.ForEach(sch =>
                {
                    Schedules.Add(new ScheduleModel(sch));
                });
                //System.Windows.MessageBox.Show(Schedules.Count.ToString());
            }
        }
        //public selectedAboment 
        public ScheduleViewModel(int abonId)
        {
            abonementId = abonId;
            db = new FitnessContext();
            AllSchedules = new ObservableCollection<Schedule>(db.Schedule.ToList());
            Schedules = new ObservableCollection<ScheduleModel>();
            var schedulesList = db.Schedule.Where(s => s.Abonement_id == abonId).OrderBy(s => s.Date).ThenBy(s => s.Period_Id).ToList();
            schedulesList.ForEach(sch =>
            {
                Schedules.Add(new ScheduleModel(sch));
            });
            isActive = Schedules.Count < db.Abonement.Find(abonId).Exercise_count;

        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
