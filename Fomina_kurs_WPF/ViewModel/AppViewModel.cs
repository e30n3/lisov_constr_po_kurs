﻿using Fomina_kurs_WPF.BLL;
using Fomina_kurs_WPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Fomina_kurs_WPF.ViewModel
{
    class AppViewModel : INotifyPropertyChanged
    {
        private FitnessContext db;
        private int clientId;
        private int trainerId;

        public ObservableCollection<AbonementModel> Abonements { get; set; }
        public ObservableCollection<User> Users { get; set; }
        public ObservableCollection<Schedule> AllSchedules { get; set; }
        public ObservableCollection<Schedule> Schedules { get; set; }
        //public ObservableCollection<Schedule> schedule { get; set; }
        private Schedule selectedSchedule;
        private AbonementModel selectedAbonement;
        public Schedule SelectedSchedule
        {
            get { return selectedSchedule; }
            set
            {
                selectedSchedule = value;
                System.Windows.MessageBox.Show(selectedSchedule.Abonement_id.ToString());
                OnPropertyChanged("SelectedSchedule");
            }
        }
        public AbonementModel SelectedAbonement
        {
            get { return selectedAbonement; }
            set { 
                selectedAbonement = value;
                OnPropertyChanged("SelectedAbonement");

                Schedules.Clear();
                List<Schedule> tmpSchedule = db.Schedule.Where(s => s.Abonement_id == selectedAbonement.Id).ToList();
                tmpSchedule.ForEach(sch =>
                {
                    Schedules.Add(sch);
                });
                //System.Windows.MessageBox.Show(Schedules.Count.ToString());
            }
        }
        //public selectedAboment 

        public AppViewModel(int client_id)
        {
            db = new FitnessContext();
            clientId = client_id;
            Abonements = new ObservableCollection<AbonementModel>();
            List<Abonement> allAbonements = db.Abonement.Where(a => a.Client_id == clientId).ToList();
            allAbonements.ForEach(abon =>
            {
                Abonements.Add(new AbonementModel(abon, db));
            });
            Users = new ObservableCollection<User>(db.User.ToList());
            Schedules = new ObservableCollection<Schedule>();
            AllSchedules = new ObservableCollection<Schedule>(db.Schedule.ToList());
            //schedule = db.Schedule.Where(s => s.Abonement_id == abonId).ToList();
        }

        public AppViewModel(int client_id, int trainer_id)
        {
            db = new FitnessContext();
            clientId = client_id;
            trainerId = trainer_id;
            Abonements = new ObservableCollection<AbonementModel>();
            List<Abonement> allAbonements = db.Abonement.Where(a => a.Client_id == clientId && a.Trainer_id == trainerId).ToList();
            allAbonements.ForEach(abon =>
            {
                Abonements.Add(new AbonementModel(abon, db));
            });
            Users = new ObservableCollection<User>(db.User.ToList());
            Schedules = new ObservableCollection<Schedule>();
            AllSchedules = new ObservableCollection<Schedule>(db.Schedule.ToList());
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
