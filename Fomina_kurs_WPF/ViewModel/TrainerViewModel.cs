﻿using Fomina_kurs_WPF.BLL;
using Fomina_kurs_WPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Fomina_kurs_WPF.ViewModel
{
    class TrainerViewModel : INotifyPropertyChanged
    {
        private FitnessContext db;
        public ObservableCollection<TrainerModel> Trainers { get; set; }
        private TrainerModel selectedTrainer;
        public TrainerModel SelectedTrainer
        {
            get { return selectedTrainer; }
            set
            {
                selectedTrainer = value;
                //TODO: Logic
                OnPropertyChanged("SelectedTrainer");
            }
        }
        public TrainerViewModel()
        {
            db = new FitnessContext();
            Trainers = new ObservableCollection<TrainerModel>();
            var trainers = db.Trainer.ToList();
            trainers.ForEach(t =>
            {
                Trainers.Add(new TrainerModel(t));
            });
            //SelectedTrainer = Trainers[0];
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
