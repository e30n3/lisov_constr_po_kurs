﻿using Fomina_kurs_WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fomina_kurs_WPF.BLL
{
    public class TrainerModel
    {
        //public Abonement trement { get; set; }
        FitnessContext db;
        public TrainerModel(Trainer tr)
        {
            db = new FitnessContext();
            User_id = tr.User_id;
            FIO = tr.FIO;
            Service_id = tr.Service_id;
            Salary = tr.Salary;
            Count_of_exercises = tr.Count_of_exercises;
            Service_name = db.Service.Find(Service_id).Name;

        }
        public string   Service_name { get; set; }
        public int      User_id { get; set; }
        public string   FIO { get; set; }
        public int      Service_id { get; set; }
        public int      Salary { get; set; }
        public int      Count_of_exercises { get; set; }
    }
}
