﻿using Fomina_kurs_WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fomina_kurs_WPF.BLL
{
    public class ScheduleModel
    {
        FitnessContext db;
        public ScheduleModel(Schedule schedule)
        {
            db = new FitnessContext();
            Period_time = db.PERIOD.ToList().Find(p => p.Id == schedule.Period_Id).Period;
            Id = schedule.Id;
            Abonement_id = schedule.Abonement_id;
            Period_Id = schedule.Period_Id;
            Date = schedule.Date.ToShortDateString();
        }
        public int Id { get; set; }
        public int Abonement_id { get; set; }
        public string Period_time { get; set; }
        public int Period_Id { get; set; }
        public string Date { get; set; }
    }
}
