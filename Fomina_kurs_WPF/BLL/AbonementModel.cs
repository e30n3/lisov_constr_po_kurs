﻿using Fomina_kurs_WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fomina_kurs_WPF.BLL
{
    public class AbonementModel
    {
        //public Abonement abonement { get; set; }
        
        public AbonementModel(Abonement abon, FitnessContext db)
        {
            Id = abon.Id;
            Exercise_count = abon.Exercise_count;
            Date_of_sale = abon.Date_of_sale;
            Price = abon.Price;
            Client_FIO = db.Client.ToList().Find(c => c.User_id == abon.Client_id).FIO;
            Client_id = db.Client.ToList().Find(c => c.User_id == abon.Client_id).User_id;
            Service_name = db.Service.ToList().Find(s => s.Id == abon.Service_id).Name;
            Trainer_FIO = db.Trainer.ToList().Find(t => t.User_id == abon.Trainer_id).FIO;

            int ex_visited = db.Schedule.Where(sch => sch.Abonement_id == abon.Id).Count();
            Ex_left = ex_visited.ToString() + " / " + Exercise_count.ToString();

        }
        public string Ex_left{ get; set; }
        public int Id { get; set; }
        public string Client_FIO{ get; set; }
        public int Client_id { get; set; }
        public string Service_name { get; set; }
        public string Trainer_FIO { get; set; }
        public int Exercise_count { get; set; }
        public DateTime Date_of_sale { get; set; }
        public int Price { get; set; }

    }
}
