﻿using Fomina_kurs_WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fomina_kurs_WPF.BLL
{
    public class TrainerScheduleRowModel
    {
        FitnessContext db;
        
        public ScheduleWeek bookedSchedules { get; }
        public TrainerScheduleRowModel(int trainer_id, int period_id, DateTime start_date, DateTime end_date)
        {
            db = new FitnessContext();
            List<Schedule> selectedPeriodInEveryDay = db.Schedule.Where(sch =>
            sch.Abonement.Trainer_id == trainer_id
            &&
            sch.Period_Id == period_id
            &&
            sch.Date >= start_date
            &&
            sch.Date <= end_date
            ).OrderBy(sch => sch.Date).ToList();

            List<bool> tmpSchedules = new List<bool>();
            for (DateTime date = start_date ; date < end_date; date = date.AddDays(1))
            {
                if (selectedPeriodInEveryDay.FirstOrDefault(sch => sch.Date == date) != null)
                {
                    tmpSchedules.Add(false);
                }
                else
                {
                    tmpSchedules.Add(true);
                }
            }
                //bookedSchedules = new ScheduleWeek(tmpSchedules);

            db = new FitnessContext();
            //Id = schedule.Id;
            //Abonement_id = schedule.Abonement_id;
            //Period_Id = schedule.Period_Id;
            //Period = db.PERIOD.ToList().Find(p => p.Id == schedule.Period_Id).Period;
            //Date = schedule.Date.ToShortDateString();
        }
        public int Id { get; set; }

        public int Abonement_id { get; set; }

        public string Period { get; set; }
        public int Period_Id { get; set; }

        public string Date { get; set; }
    }
}
