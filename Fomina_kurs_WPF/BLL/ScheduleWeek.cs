﻿using Fomina_kurs_WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fomina_kurs_WPF.BLL
{
    public class ScheduleWeek
    {
        private FitnessContext db;
        public string Period_name { get; set; }
        public byte[] Days { get; set; }
        public string[] Content { get; set; }
        public ScheduleWeek(int client_id, int tr_id, int period_id, DateTime start_date)
        {
        
            db = new FitnessContext();
            Period_name = db.PERIOD.Find(period_id).Period;
            Days = new byte[7];
            Content = new string[7];
            DateTime end_date = start_date.AddDays(14);
            List<Schedule> selectedPeriodInEveryDay = db.Schedule.Where(sch =>
            sch.Abonement.Trainer_id == tr_id
            &&
            sch.Period_Id == period_id
            &&
            sch.Date >= start_date
            &&
            sch.Date <= end_date
            ).OrderBy(sch => sch.Date).ToList();
            for (int i = 0; i < 7; i++)
            {
                if (selectedPeriodInEveryDay.FirstOrDefault(sch => sch.Date == start_date.AddDays(i)) != null)
                {
                    Days[i] = 0;
                } else
                {
                    Days[i] = 1;
                }
                if (selectedPeriodInEveryDay.FirstOrDefault(sch => sch.Date == start_date.AddDays(i) && sch.Abonement.Client_id == client_id) != null)
                {
                    Days[i] = 2;
                }

                    Content[i] = period_id.ToString() + "|" + start_date.AddDays(i).ToString();
            }
            //List<bool> tmpSchedules = new List<bool>();
            //for (DateTime date = start_date; date < end_date; date = date.AddDays(1))
            //{
            //    if (selectedPeriodInEveryDay.FirstOrDefault(sch => sch.Date == date) != null)
            //    {
            //        tmpSchedules.Add(false);
            //    }
            //    else
            //    {
            //        tmpSchedules.Add(true);
            //    }
            //}
        }
    }
}
