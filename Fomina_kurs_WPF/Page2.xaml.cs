﻿using Fomina_kurs_WPF.BLL;
using Fomina_kurs_WPF.Models;
using Fomina_kurs_WPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Fomina_kurs_WPF
{
    /// <summary>
    /// Логика взаимодействия для Page2.xaml
    /// </summary>
    public partial class Page2 : Page
    {
        Frame frame;
        int abonementId;
        public Page2(Frame fr, int abonId)
        {
            InitializeComponent();
            frame = fr;
            abonementId = abonId;
            this.DataContext = new ScheduleViewModel(abonId);
            

            //db = dbContext;
            //schedule = db.Schedule.Where(s => s.Abonement_id == abonId).ToList();
            //dataGridSchedule.ItemsSource = schedule;

            //System.Windows.MessageBox.Show(abonement.Client_id.ToString());
        }
        public void AddVisiting(object sender, RoutedEventArgs e)
        {
            AddVisitingForm addVisitingForm = new AddVisitingForm(abonementId);
            if (addVisitingForm.ShowDialog() == true)
            {
                this.DataContext = new ScheduleViewModel(abonementId);
                MessageBox mb = new MessageBox(true, "Посещение забронировано!");
                mb.ShowDialog();
            }
        }
    }
}
